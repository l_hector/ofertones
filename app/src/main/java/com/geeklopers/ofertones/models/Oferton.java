package com.geeklopers.ofertones.models;

/**
 * Created by Hector Arredondo on 13/12/2015.
 */
public class Oferton {
    public int id;
    public int exclusive;
    public String title;
    public String subTitle;
    public String url;
    public String description;
    public Map Map;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getExclusive() {
        return exclusive;
    }

    public void setExclusive(int exclusive) {
        this.exclusive = exclusive;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map getMap() {
        return Map;
    }

    public void setMap(Map Map) {
        this.Map = Map;
    }


}