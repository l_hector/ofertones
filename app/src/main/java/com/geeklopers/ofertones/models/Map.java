package com.geeklopers.ofertones.models;

/**
 * Created by Hector Arredondo on 20/12/2015.
 */
public class Map {
    public int hasMap;
    public double latitude;
    public double longitude;
    public double altitude;
    public String title;

    public  Map() {};

    public int getHasMap() {
        return hasMap;
    }

    public void setHasMap(int hasMap) {
        this.hasMap = hasMap;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String subTitle;
}
