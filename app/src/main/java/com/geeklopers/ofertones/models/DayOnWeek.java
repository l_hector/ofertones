package com.geeklopers.ofertones.models;

import java.util.List;

/**
 * Created by Hector Arredondo on 13/12/2015.
 */
public class DayOnWeek {
    public int id;
    public int dayOnWeek;
    public String nameDay;
    public List<Oferton> ofertonList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDayOnWeek() {
        return dayOnWeek;
    }

    public void setDayOnWeek(int dayOnWeek) {
        this.dayOnWeek = dayOnWeek;
    }

    public String getNameDay() {
        return nameDay;
    }

    public void setNameDay(String nameDay) {
        this.nameDay = nameDay;
    }

    public List<Oferton> getOfertonList() {
        return ofertonList;
    }

    public void setOfertonList(List<Oferton> ofertonList) {
        this.ofertonList = ofertonList;
    }
}
