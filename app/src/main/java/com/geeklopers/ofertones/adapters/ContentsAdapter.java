package com.geeklopers.ofertones.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.geeklopers.ofertones.models.Oferton;
import com.geeklopers.ofertones.utils.AppController;

import com.geeklopers.ofertones.R;

import java.util.List;

/**
 * Created by Hector Arredondo on 13/12/2015.
 */
public class ContentsAdapter extends BaseAdapter{

    private Context mContext;
    private ViewFolder vFolder;

    private LayoutInflater inflater;
    private List<Oferton> ofertonList;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public ContentsAdapter(Context context, List<Oferton> ofertonList) {
        this.mContext = context;
        this.ofertonList = ofertonList;
    }

    @Override
    public int getCount() {
        return ofertonList.size();
    }

    @Override
    public Object getItem(int position) {
        return ofertonList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = convertView;
        final int localPosition = position;

        if(item == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            item = inflater.inflate(R.layout.item_content, null);

            vFolder = new ViewFolder();
            vFolder.imgIsExclusive = (ImageView) item.findViewById(R.id.imgIsExclucive);

            item.setTag(vFolder);
        }

        vFolder = (ViewFolder) item.getTag();

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        if (ofertonList.get(position).getExclusive() == 1)
            vFolder.imgIsExclusive.setVisibility(View.VISIBLE);
        else
            vFolder.imgIsExclusive.setVisibility(View.GONE);


        NetworkImageView thumbNail = (NetworkImageView) item.findViewById(R.id.thumbnail);
        TextView txtTitle = (TextView) item.findViewById(R.id.title);

        thumbNail.setImageUrl(ofertonList.get(position).getUrl(), imageLoader);
        txtTitle.setText(ofertonList.get(position).getTitle());
        return item;
    }

    public class ViewFolder {
        public ImageView imgIsExclusive;
    }
}
