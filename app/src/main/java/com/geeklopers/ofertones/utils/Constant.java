package com.geeklopers.ofertones.utils;

/**
 * Created by Hector Arredondo on 13/12/2015.
 */
public class Constant {
    //public static final String URL_SERVER = "http://geeklopers.com/ofertones/ofertones2.php?request=getSales";
    public static final String URL_SERVER = "http://ofertonesapp.com/api/sales?request=getSales";
    public static final String URL_GEEKLOOPERS = "http://geeklopers.com/";
}
