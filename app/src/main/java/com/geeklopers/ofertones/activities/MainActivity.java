package com.geeklopers.ofertones.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.geeklopers.ofertones.R;
import com.geeklopers.ofertones.adapters.ContentsAdapter;
import com.geeklopers.ofertones.models.Map;
import com.geeklopers.ofertones.models.Oferton;
import com.geeklopers.ofertones.models.DayOnWeek;
import com.geeklopers.ofertones.utils.AppController;
import com.geeklopers.ofertones.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private static Category categoriesList = new Category();

    private Toolbar toolbar;

    private List<Oferton> ofertonList = new ArrayList<Oferton>();
    private ListView listView;
    private ContentsAdapter adapter;
    private RelativeLayout lySplash;
    private RelativeLayout lyContent;
    private ProgressBar progressBar;
    private Button btnCalendar;

    private  Dialog dialog;
    private PopupWindow pwindow = null;

    private MenuItem menuItem;

    private int dayOnWeek = 0;
    private String[] sDays = {"Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        lySplash = (RelativeLayout) findViewById(R.id.lySplash);
        lyContent = (RelativeLayout) findViewById(R.id.lyOfertas);
        listView = (ListView) findViewById(R.id.listCont);
        adapter = new ContentsAdapter(this, ofertonList);
        listView.setAdapter(adapter);
        btnCalendar = (Button) findViewById(R.id.btnCalendar);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, OfertonDetilsActivity.class);
                Oferton lOferton = categoriesList.getSales().get(dayOnWeek).getOfertonList().get(position);

                intent.putExtra("url", lOferton.getUrl());
                intent.putExtra("title", lOferton.getTitle());
                intent.putExtra("description", lOferton.getDescription());

                intent.putExtra("longitude", lOferton.getMap().getLatitude());
                intent.putExtra("latitude", lOferton.getMap().getLongitude());
                intent.putExtra("mapTitle", lOferton.getMap().getTitle());
                intent.putExtra("mapSubTitle", lOferton.getMap().getSubTitle());
                intent.putExtra("isExclusive", lOferton.getExclusive());
                startActivity(intent);
            }
        });

        btnCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPoppup();
            }
        });

        dialog = new Dialog(this);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        progressBar.animate();

        sendRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        menuItem = menu.getItem(0);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.greekDevelopers:
                Intent intent = new Intent(MainActivity.this, GeeklopersActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendRequest() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Constant.URL_SERVER, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                if (lySplash.getVisibility() == View.VISIBLE) {
                    progressBar.clearAnimation();

                    lySplash.setVisibility(View.GONE);
                    lyContent.setVisibility(View.VISIBLE);
                    menuItem.setVisible(true);
                }

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONArray categoryJSONObject = response.getJSONArray("categories");
                    List<DayOnWeek> salesList = new ArrayList<>();
                    Category category = new Category();

                    if (lySplash.getVisibility() == View.VISIBLE) {
                        progressBar.clearAnimation();

                        lySplash.setVisibility(View.GONE);
                        lyContent.setVisibility(View.VISIBLE);
                        menuItem.setVisible(true);
                    }

//                    // Parsing json
                    for (int i = 0; i < categoryJSONObject.length(); i++) {
                        try {

                        JSONObject obj = categoryJSONObject.getJSONObject(i);
                        DayOnWeek dayOnWeek = new DayOnWeek();

                        // Genre is json array
                        int id = obj.getInt("id");
                        String name = obj.getString("name");
                        JSONArray salesJSONArray = obj.getJSONArray("sales");
                        ArrayList<Oferton> ofertonsList = new ArrayList<>();

                        for(int j = 0; j < salesJSONArray.length(); j++) {
                            JSONObject salesJSONObject = salesJSONArray.getJSONObject(j);

                            Oferton oferton = new Oferton();
                            oferton.setTitle(salesJSONObject.getString("title"));
                            oferton.setSubTitle(salesJSONObject.getString("subtitle"));
                            oferton.setId(salesJSONObject.getInt("id"));
                            oferton.setExclusive(salesJSONObject.getInt("exclusive"));
                            oferton.setUrl(salesJSONObject.getString("url"));
                            oferton.setDescription(salesJSONObject.getString("description"));

                            JSONObject mapObject = salesJSONObject.getJSONObject("map");
                            Map map = new Map();
                            map.setLatitude(mapObject.getDouble("latitud"));
                            map.setLongitude(mapObject.getDouble("altitude"));
                            map.setTitle(mapObject.getString("title"));
                            map.setSubTitle(mapObject.getString("subtitle"));
                            map.setHasMap(mapObject.getInt("hasMap"));

                            oferton.setMap(map);
                            ofertonsList.add(oferton);
                        }

                        dayOnWeek.setId(id);
                        dayOnWeek.setNameDay(name);
                        dayOnWeek.setOfertonList(ofertonsList);

                        salesList.add(dayOnWeek);

                        category.setSales(salesList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    MainActivity.categoriesList = category;
                    int day = getDayInt();
                    setSales(day);
                    btnCalendar.setText(sDays[day]);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void setSales(int pPayOnWeek) {
        this.dayOnWeek = pPayOnWeek;
        List<DayOnWeek> dayOnWeek = categoriesList.getSales();

        this.ofertonList.clear();

        this.ofertonList.addAll(dayOnWeek.get(pPayOnWeek).getOfertonList());
        adapter.notifyDataSetChanged();

        this.listView.setSelection(0);
    }

    private int getDayInt() {
        int day = 0;
        Calendar calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_WEEK);
        day -= 1;
        day = day == -1 ? 6 : day;

        return day;
    }

    public void openDialogNextTime(View v) {

        //dialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_next_time, null);
        Button btnCerrar = (Button)view.findViewById(R.id.btnCerrarDialog);

        dialog.setContentView(view);
        dialog.setTitle(this.getResources().getString(R.string.dialog_next_time_title));
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showPoppup() {
        //if (pwindow == null) {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(this.LAYOUT_INFLATER_SERVICE);
            View layoutt = inflater.inflate(R.layout.dialog_menu_day, null);

            pwindow = new PopupWindow(layoutt, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, false);
        //}
        pwindow.showAtLocation(lyContent, Gravity.CENTER,  0, 0);
    }

    public void onDayEvent(View v) {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.cancel();
            }
        }

        if (pwindow.isShowing()) {
            pwindow.dismiss();
        }

        int day = Integer.parseInt(v.getTag().toString());
        btnCalendar.setText(sDays[day]);
        dayOnWeek = day;

        setSales(Integer.parseInt(v.getTag().toString()));
    }

    @Override
    public void onBackPressed() {
        if (pwindow != null) {
            if (pwindow.isShowing()) {
                pwindow.dismiss();
               // pwindow.notify();
            }else
                super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    public static  class Category {
        public int vertion;
        public int day;
        public List<DayOnWeek> dayOnWeeks;

        public int getVertion() {
            return vertion;
        }

        public void setVertion(int vertion) {
            this.vertion = vertion;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public List<DayOnWeek> getSales() {
            return dayOnWeeks;
        }

        public void setSales(List<DayOnWeek> dayOnWeeks) {
            this.dayOnWeeks = dayOnWeeks;
        }
    }
}
