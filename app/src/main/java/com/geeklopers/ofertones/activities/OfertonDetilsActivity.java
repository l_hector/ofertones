package com.geeklopers.ofertones.activities;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.geeklopers.ofertones.R;
import com.geeklopers.ofertones.models.Oferton;
import com.geeklopers.ofertones.utils.AppController;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class OfertonDetilsActivity extends AppCompatActivity {

    private ImageView img;
    private ImageView imgExclusive;
    private TextView title;
    private TextView description;
    private GoogleMap mapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oferton_detils);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {// Habilitar up button{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    OfertonDetilsActivity.this.finish();
                    Toast.makeText(OfertonDetilsActivity.this, "click", Toast.LENGTH_LONG).show();
                }
            });
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OfertonDetilsActivity.this.finish();
            }
        });

        String url = getIntent().getStringExtra("url");
        String sTitle = getIntent().getStringExtra("title");
        String sDescription = getIntent().getStringExtra("description");
        double sLatitude = getIntent().getDoubleExtra("longitude", 24.8086541);
        double sLongitude = getIntent().getDoubleExtra("latitude", -107.396172);
        String sMapTitle = getIntent().getStringExtra("mapTitle");
        String sMapSubtitle = getIntent().getStringExtra("mapSubTitle");
        int isExclusive = getIntent().getIntExtra("isExclusive", 0);
        LatLng marker = new LatLng(sLatitude, sLongitude);

       toolbar.setTitle(sTitle);

        NetworkImageView thumbNail = (NetworkImageView) findViewById(R.id.img);
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        imgExclusive = (ImageView) findViewById(R.id.imgIsExcluciveDetails);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        mapa = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMap();
        mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mapa.setMyLocationEnabled(true);
        mapa.getUiSettings().setZoomControlsEnabled(false);
        mapa.getUiSettings().setCompassEnabled(true);
        mapa.getUiSettings().setCompassEnabled(true);
        mapa.getUiSettings().setMapToolbarEnabled(true);
        mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 15));
        mapa.addMarker(new MarkerOptions()
                .position(marker)
                .title(sMapTitle)
                .snippet(sMapSubtitle)
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.marker))
                .anchor(0.5f, 0.5f));

        thumbNail.setImageUrl(url, imageLoader);
        title.setText(sTitle);
        description.setText(sDescription);
        if(isExclusive == 1)
            imgExclusive.setVisibility(View.VISIBLE);

        getSupportActionBar().setTitle(sTitle);
    }

}
